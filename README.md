# CORmodel

COR, a zero-dimensional Lagrangian model where a small element of the jet burnt gases mixes at a certain rate with the fresh gases while it potentially ignites [1].

[1] Quentin Malé et al. "Direct numerical simulations and models for hot burnt gases jet ignition", Combustion and Flame, https://doi.org/10.1016/j.combustflame.2020.09.017

## Description
COR consists in following an open reactor, small enough to be homogeneous, initially full of burnt gases, convected into an atmosphere. These hot burnt gases mix at a certain rate with the cold fresh gases of the atmosphere within this open reactor and chemistry inside the reactor are tracked using an ODE solver with chemical kinetics. In this approach, the mixture fraction between the hot jet and the cold gases in the open reactor changes according to a mixing rate.

## Installation
You can simply add the path toward CORmodel to your PYTHONPATH.
Cantera is required for thermochemistry computations.

## Usage
Use examples in the examples folder to get familiar with the code.

