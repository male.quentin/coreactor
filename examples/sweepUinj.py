"""Example of injection speed sweep. Methane/air mixture."""
import numpy as np
import cantera as ct

from CORMODEL.COReactor import COR_model


ct.suppress_thermo_warnings()

# parameters
dinj = 2.e-3  # 2 mm duct diameter
pressure = 101325.  # 1 atm.

# cantera solution objects
burnt_gas = ct.Solution("reducedS37R129_0.cti")
fresh_gas = ct.Solution("reducedS37R129_0.cti")
gas = ct.Solution("reducedS37R129_0.cti")

# set burnt gas, lean CH4/air mixture
burnt_gas.TP = 298., pressure
burnt_gas.set_equivalence_ratio(0.9, 'CH4', 'O2:1,N2:3.76')
burnt_gas.equilibrate('HP')
burnt_gas.TPY = burnt_gas.T*0.85, burnt_gas.P, burnt_gas.Y

# set fresh gas, lean CH4/air mixture
fresh_gas.TP = 298., pressure
fresh_gas.set_equivalence_ratio(0.8, 'CH4', 'O2:1,N2:3.76')

for Uinj in np.linspace(10., 300., 29):
    # call external function COR_model to compute the result of the COR
    # modelling approach
    ignition = COR_model(gas, P=pressure, Tu=fresh_gas.T, Yu=fresh_gas.Y,
                         Tb=burnt_gas.T, Yb=burnt_gas.Y, Uinj=Uinj, dinj=dinj)
